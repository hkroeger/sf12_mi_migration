


*********************************
**** MENTAL HEALTH VARIABLES ****
*********************************



*** SF-12:      bgp105-bgp10810
*** PHQ-4:      bgp0301 bgp0302 bgp0303 bgp0304
*** Loneliness: bgpr321 bgpr322 bgpr323 (only refugees in 2016), SOEP-CORE in 2013
*** BRCS:


/*
(1) bgpr345: I try to think of how I can change difficult situations.
(2) bgpr346: No matter what happens to me, I think I have my reactions under control.
(3) bgpr347: I think I can develop further if I deal with difficult situations.
(4) bgpr348: I actively seek ways to balance out the losses that have affected my life.

*/
*** BIG-5:      

/*
*** loneliness SOEP-CORE ***

use persnr pid hhnrakt   bdp07* using "${SOEP}bdp.dta", clear

rename bdp0701 lone1
rename bdp0702 lone2
rename bdp0703 lone3

gen syear =2016
recode lone1-lone3 (min/-1=.)

save "${TEMP}lone-core.dta", replace
*/

*************************************
**** GET BRCS from SOEP-IS 2019 *****
*************************************






*********************************
**** MENTAL HEALTH VARIABLES ****
*********************************



*** SF-12:      bgp105-bgp10810  (includes self-rated health)
*** PHQ-4:      bgp0301 bgp0302 bgp0303 bgp0304
*** RHS15:       bhp_382_q57 und folgende


*********************************
**** Demographic VARIABLES ****
*********************************

*** zuzugsjahr
**** alter, geburtsjahr
**** bildung
**** sprache







use persnr pid hhnrakt syear sample1 bgpbirthy pspvers bgpbirthm bgp0301 bgp0302 bgp0303 bgp0304 bgp105-bgp10810 bgpr321 bgpr322 bgpr323 bgpr7201 bgpr50 bgpr3901  bgpr3902 bgp31 bgpr345-bgpr348 using "${SOEP}bgp.dta", clear



rename bgp31 erwstat

* PHQ-4
rename bgp0301 phq1
rename bgp0302 phq2
rename bgp0303 phq3
rename bgp0304 phq4

* SF-12
rename bgp105   sf12_srh
rename bgp106   sf12_stairs
rename bgp107   sf12_hinder
rename bgp10801 sf12_tp
rename bgp10802 sf12_down
rename bgp10803 sf12_relax
rename bgp10804 sf12_energy
rename bgp10805 sf12_pain
rename bgp10806 sf12_less_b
rename bgp10807 sf12_content
rename bgp10808 sf12_quant_s
rename bgp10809 sf12_care_s
rename bgp10810 sf12_social

rename bgpr7201 mother_tongue
rename pspvers survey_language


*********************************
**** MIGRATION VARIABLES     ****
*********************************


merge 1:1 persnr using "${SOEP}ppfad.dta", keep(3) nogenerate keepusing(sex psample gebjahr bgsampreg immiyearinfo corigininfo germborninfo arefback arefinfo bgnetto migback corigin)
merge 1:1 persnr using "${SOEP}phrf.dta", keep(3) nogenerate keepusing(bgphrf)
merge 1:1 persnr using "${SOEP}bgpgen.dta", keep(1 3) nogenerate keepusing(lfs16)
merge 1:1 persnr using "${SOEP}bgpbrutto.dta", keep(3) nogenerate keepusing(bgbefstat)

rename lfs16 lfs



**************************
**** FLUCHT VARIABLES ****
**************************


merge 1:1 persnr using "${SOEP}bgp_refugees.dta", keep(1 3)  nogenerate keepusing(bgpr0101 bgpr_l_28 bgpr_l_3301 bgpr_l_3302 ///
 bgpr_l_3303 bgpr_l_3304 bgpr_l_3305 bgpr_l_3306 bgpr_l_3307 bgpr_l_3308 bgpr_l_51 bgpr_l_52 bgpr3903 bgpr_l_3401 bgpr_l_3402 bgprmonin bgpr87 bgpr88 bgpr89 bgpr_l_0301 bgpr_l_0319)

 
 rename bgpr3903 noasyl
 
 *Herkunftsgruppen
gen staat3=.
label var staat3 "Staatsangehoerigkeitsgruppen"
	replace staat3=1 if bgpr0101==19
	replace staat3=2 if bgpr0101==1
	replace staat3=3 if bgpr0101==10
	replace staat3=4 if inlist(bgpr0101,2,12,17)
	replace staat3=5 if inlist(bgpr0101,6,18)
	replace staat3=6 if inlist(bgpr0101,11,15)
	replace staat3=7 if inlist(bgpr0101,3,4,5,7,8,9,13,14,16,20,21,22)
label define staat3_label  1 "Syrien"  2 "Afghanistan"  3 "Irak"  ///
						   4 "Albanien, Serbien, Kosovo"  ///
						   5 "Eritrea, Somalia"  6 "Iran, Pakistan"  ///
						   7 "Sonstige", modify
label values staat3 staat3_label

*Aufenthaltsdauer in Monaten
recode bgpr_l_3401 bgpr_l_3402 (min/-1=.)
gen aufenth_j=syear-bgpr_l_3401
gen aufenth_m=bgprmonin-bgpr_l_3402
gen aufenth=(aufenth_j*12)+aufenth_m
label var aufenth "Aufenthaltsdauer in Monaten"
*Aufenthaltsdauer - gruppiert
recode aufenth 	(0/12=1 "erste jahr") (13/24=2 "zweite jahr") ///
				(25/max=3 "dritte Jahr und mehr"), gen(aufenthj)	


 rename bgpr_l_0301 region_afghan
 rename bgpr_l_0319 region_syria
 

*A.2 Sprachkenntnisse Deutsch 2016
recode  bgpr87 bgpr88 bgpr89 (min/-1=.)
revrs bgpr87 bgpr88 bgpr89

alpha revbgpr87 revbgpr88 revbgpr89, gen(language_index)
label var language_index "Index Deutschkenntnisse zur Befragung"
renames revbgpr87 revbgpr88 revbgpr89 \ speaking writing reading





recode phq1-phq4 (min/-1=.)

cap drop phq4_score
egen phq4_score = rowtotal(phq1 phq2 phq3 phq4)
replace phq4_score = phq4_score -4

egen phq_check = rownonmiss(phq1 phq2 phq3 phq4)

replace phq4_score =. if phq_check !=4




save "${TEMP}rhr-data.dta", replace






**** use the data preparation done in the RHR-project ****
use "${TEMP}rhr-data.dta", clear




merge 1:1 pid syear using "${SOEPlong}pl.dta", keep( 1 2 3) keepusing(plj0665 plh0212-plh0226 plh0245-plh0252 plj0587-plj0589 plh0271 plh0270 plh0269) nogenerate

keep if syear >=2016

**** BIG-5 *****


rename plh0212 big5_thorough
rename plh0213 big5_comm
rename plh0214 big5_coarse
rename plh0215 big5_original
rename plh0216 big5_worry
rename plh0217 big5_forgive
rename plh0218 big5_lazy
rename plh0219 big5_sociable
rename plh0220 big5_art
rename plh0221 big5_nervous
rename plh0222 big5_efficient
rename plh0223 big5_reserved
rename plh0224 big5_friendly
rename plh0225 big5_lively
rename plh0226 big5_stress

**** LOCUS OF CONTROL *****

rename plh0245 locus_lifescourse
rename plh0246 locus_luck
rename plh0247 locus_others
rename plh0248 locus_success
rename plh0249 locus_doubt
rename plh0250 locus_social
rename plh0251 locus_effort
rename plh0252 locus_control


recode big5_* locus_*  (-9/-1=.)

egen check_big5 = rownonmiss(big5* )
egen check_locus = rownonmiss(locus* )

cap drop _merge

rename plj0587 lone1
rename plj0588 lone2
rename plj0589 lone3

replace lone1 = plh0269 if lone1 <0 & plh0269 >0
replace lone2 = plh0270 if lone2 <0 & plh0270 >0
replace lone3 = plh0271 if lone3 <0 & plh0271 >0

xtset pid syear
forvalues X = 1/3 {
	replace lone`X' = f.lone`X' if lone`X' <0 & psample <17 & syear ==2016
}

keep if syear ==2016

rename bgpr345 brcs1
 rename bgpr346 brcs2
 rename bgpr347 brcs3
 rename bgpr348 brcs4

*append using "${SOEPlong}brcs-is.dta", generate(is)

/*
(1) bgpr345: I try to think of how I can change difficult situations.
(2) bgpr346: No matter what happens to me, I think I have my reactions under control.
(3) bgpr347: I think I can develop further if I deal with difficult situations.
(4) bgpr348: I actively seek ways to balance out the losses that have affected my life.

*/

 
 
 
 



gen migstatus     = 1 if migback ==1
replace migstatus = 2 if migback ==3
replace migstatus = 3 if migback ==2
replace migstatus = 4 if psample >= 17 & psample <=18


label define migstatus 1 "No Mig." 2 "Indirect Mig." 3 "Direct Mig." 4 "Refugees"
label values migstatus migstatus

recode lone1-lone3 (min/-1=.)


*** Status im Asylverfahren
rename bgpr50 asylstat
rename bgpr3901 arrivy // Jahr Ankunft
rename bgpr3902 arrivm // Monat Ankunft
//hallo
recode arrivy (min/-1=.)
*rename plj0665 noasyl

**** country groups ****
recode corigin (1=1 "Germany") (2=2 "Turkey") (22 = 3 "Poland") (21 = 4 "Romania") ///
(30 = 5 "Syria") (32 = 6 "Russia") (43 = 7 "Afghanistan") (60 = 8 "Irak") (3 119/122 165 168 = 9 "Ex-Yugoslavia") ///
 (4/6 28 75 119 140 = 10 "Southern Europe")   (26 29 31 73 78 101 103 123 132 146 222 = 11 "Eastern Europe") (74 77 82 97 130 141 148 155 = 12 "CIS")  (46 52 67 76 79 81 87 90 111 126 151 152 = 13 "Arabic")  ///
 (20 27 34 35 48 51 61 64 88 96 108 109 133 170 171=  14 "Latin America") (10/19 41 55 56 71 116 117 118= 15 "Western Europe & NA") ///
  (47 49 54 80 84 86 89 94 102 105 110 113 123 125 127 135 138 139 142/144 147 158 162 166 173 176 178 183 = 16 "Africa")  (23 25 38 40 42 44 50 65 66 68 83 85 93 100 128 145 154 169 24 = 17 "Asia") (45 53 59 129 39 = 18 "Rest")  (-1 999 98=.),gen(co_group)

 label variable co_group "Country of origin, grouped"
 

recode _all (min/-1=.)

*identify "non-representative sample"
gen no_repr = 0
label var no_repr "Arrived before 2013; Not refugee"
replace no_repr = 1 if arrivy < 2013 | arrivy >2016
*kein Asylverfahren durchlaufen & sonstigen Aufenthaltstitel (nicht "Flucht")
replace no_repr = 1 if noasyl == 1 & asylstat == 8
*identified by SOEP as nicht-flüchtlinge, Migrationsfragebogen wurde eingestetzt
*replace no_repr = 1 if instrument==95
replace no_repr =-1 if (psample !=17 & psample != 18)

drop if no_repr ==1

*******************************************************
**** DEFINING WHICH GROUPS TO KEEP            *********
*******************************************************
*keep only groups of n >= 100 per gender
*** define minum, new: 50
local minsize = 51

 

 **** 3 types of analyses: 1) mig_status [no minsize necessary] 2) country of origin 3) survey_language
*** exclude all groups of countries
*** survey language genderXlanguage: no women for Urdu/Pashtu

bysort corigin sex migstatus: gen N_country = _N 
gen co_group2 = co_group 
replace co_group2 =. if N_country >=`minsize'
bysort co_group2 sex migstatus: gen N_group = _N  
drop co_group2

*** if possible by country of origin, if not by grouped country of origin

gen group_analysis = corigin if N_country >= `minsize' & N_country !=.
*replace group_analysis = co_group*100 if group_analysis ==. &  N_group >=`minsize' & N_group !=.
 
 
 *label define corigin 900 "[900] Ex-Yugoslavia" 1000 "[1000] Southern Europe" 1100 "[1100] Eastern Europe" 1200 "[1200] CIS" 1500 "[1500] Western Europe & NA" 1600 "[1600] Africa" 1700 "[1700] Asia", add
 label values group_analysis corigin

 recode migstatus (2=1),gen(migstat)
 
 replace survey_language =0 if migstatus <4
 
 gen gender_group = sex*10000+group_analysis
 gen gender_language = sex*10+survey_language
  gen gender_migstatus = sex*10+migstat
  
  *** too few observations for pashtu and  Urdu+female
	replace gender_language =. if inlist(gender_language,14,24,25)
 **markers for all missings on mental health variables
 rename sf12_tp time_pressure
 move time_pressure syear
 
 egen check_phq  = rownonmiss(phq1-phq4)
 egen check_sf12 = rownonmiss(sf12*)
 egen check_lone = rownonmiss(lone*)

 *keep if check_phq >=1
 ***** check if all values are available in all indicators in all groups:
 
 bysort gender_group: sum phq* sf12*
 gen  phrf = bgphrf
 sum phrf 
 gen phrf_stand = phrf/`r(mean)' 
 
gen age = 2016-gebjahr 

 
 drop bgpr321 bgpr322 bgpr323
 
 recode age (18/24 = 1 "18-24") (25/29= 2 "25-29")  (30/34=3  "30-34") ///
								(35/39= 4 "35-39")  (40/44=5  "40-44") ///
								(45/49= 6 "45-49")  (50/54=7  "50-54") ///
								(55/59= 8 "55-59")  (60/64=9  "60-64") ///
								(65/69= 10 "65-69") (70/74=11 "70-74") ///
								(75/max= 12 "75+"), gen(age_cat)
								
								recode age_lone (18/24 = 1 "18-24") (25/29= 2 "25-29")  (30/34=3  "30-34") ///
								(35/39= 4 "35-39")  (40/44=5  "40-44") ///
								(45/49= 6 "45-49")  (50/54=7  "50-54") ///
								(55/59= 8 "55-59")  (60/64=9  "60-64") ///
								(65/69= 10 "65-69") (70/74=11 "70-74") ///
								(75/max= 12 "75+"), gen(age_cat_lone)
								
								*18-24;25-29;30-34;35-39;40-44;45-49;50-54;55-59;60-64;65-69;70-74;75+
 
 

 recode group_analysis (.=9999)
 recode gender_group (.=999999)
 replace group_analysis =1 if migstat ==1
 
 
 saveold "${TEMP}DMHM-MI-`c(current_date)'.dta",replace version(12)

 
