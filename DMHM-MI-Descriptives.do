*********************************
* Hannes Kröger & Ana Tibubos   *
* analysis file 2019/01/21		*
* 								*
* MHM-MI       file for   *
* descriptive tables fo          *
* establishing measurement      *
* invariance for mental items   *
* across German host population *
* migrants and refugees         *
* 				   				*
*********************************

set more off
use "${TEMP}DMHM-MI-16 Dec 2019.dta", clear


**** total age range in sample ****

sum age if group_analysis !=. & (check_phq !=. | check_sf12 !=.) & no_repr !=1

keep if group_analysis !=. & (check_phq !=. | check_sf12 !=.) & no_repr !=1

      **** TABLE 1 ****
****************************************
*** Number of observations per cell ****
****************************************
tab group_analysis, gen(ga)
recode ga1-ga16 (0=.)
mat A = J(1,16,.)
mat S = J(1,16,.)
***germans
estpost summarize ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14 if sex == 1  & migstatus<=2,detail
forvalues X = 1/16 {
	sum age if ga`X' ==1 & sex == 1  & migstatus<=2
	if `r(N)' != 0{
		mat A[1,`X'] = `r(mean)'
		mat S[1,`X'] = `r(sd)'
	}
}
matrix  colnames A = ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14
matrix  colnames S = ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14
*estadd matrix A
*estadd matrix S
est store cells_men_ger
mat A = J(1,16,.)
mat S = J(1,16,.)
estpost summarize ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14 if sex == 2  & migstatus<=2,detail
forvalues X = 1/16 {
	sum age if ga`X' ==1 & sex == 2  & migstatus<=2
	if `r(N)' != 0{
		mat A[1,`X'] = `r(mean)'
		mat S[1,`X'] = `r(sd)'
	}
}
matrix  colnames A = ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14
matrix  colnames S = ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14
*estadd matrix A
*estadd matrix S
est store cells_women_ger
mat A = J(1,16,.)
mat S = J(1,16,.)
*** migrants
estpost summarize ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14 if sex == 1  & migstatus==3,detail
forvalues X = 1/16 {
	sum age if ga`X' ==1 & sex == 1  & migstatus==3
	if `r(N)' != 0{
		mat A[1,`X'] = `r(mean)'
		mat S[1,`X'] = `r(sd)'
	}
}
matrix  colnames A = ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14
matrix  colnames S = ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14
*estadd matrix A
*estadd matrix S
est store cells_men_mig
mat A = J(1,16,.)
mat S = J(1,16,.)

estpost summarize ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14 if sex == 2  & migstatus==3,detail
forvalues X = 1/16 {
	sum age if ga`X' ==1 & sex == 2  & migstatus==3
	if `r(N)' != 0{
		mat A[1,`X'] = `r(mean)'
		mat S[1,`X'] = `r(sd)'
	}
}
matrix  colnames A = ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14
matrix  colnames S = ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14
*estadd matrix A
*estadd matrix S
est store cells_women_mig
mat A = J(1,16,.)
mat S = J(1,16,.)

*** refugees
estpost summarize ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14 if sex == 1  & migstatus==4,detail
forvalues X = 1/16 {
	sum age if ga`X' ==1 & sex == 1  & migstatus==4
	if `r(N)' != 0{
		mat A[1,`X'] = `r(mean)'
		mat S[1,`X'] = `r(sd)'
	}
}
matrix  colnames A = ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14
matrix  colnames S = ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14
*estadd matrix A
*estadd matrix S
est store cells_men_ref
mat A = J(1,16,.)
mat S = J(1,16,.)
estpost summarize ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14 if sex == 2  & migstatus==4,detail
forvalues X = 1/16 {
	sum age if ga`X' ==1 & sex == 2  & migstatus==4
	if `r(N)' != 0{
		mat A[1,`X'] = `r(mean)'
		mat S[1,`X'] = `r(sd)'
	}
}
matrix  colnames A = ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14
matrix  colnames S = ga1 ga2 ga3 ga4 ga5 ga6 ga7 ga12 ga13 ga16 ga9 ga8 ga10 ga11 ga15 ga14
*estadd matrix A
*estadd matrix S
est store cells_women_ref
mat A = J(1,16,.)
mat S = J(1,16,.)


esttab  cells_women_ger cells_men_ger cells_women_mig cells_men_mig cells_women_ref cells_men_ref using "${TABLES}DMHM-MI-table1-cells-age.rtf", rtf cells("count(fmt(%06.0fc))")  replace/*
		*/ compress   title("Number of observations per group of country of origin") stats(N, fmt(0) labels( "Observations")) label ///
		mgroups("Germans" "Migrants" "Refugees", pattern(1 0 1 0 1 0)) mtitles("Women" "Men" "Women" "Men" "Women" "Men") nonumbers
 
 



*****************************************
*** Summary statistics of the sample ****
*** by Refugee/Migrants              ****
*****************************************

estpost summarize age sex ,detail

est store sstab


	
		* esttab  items using "${TABLES}DMHM-MI-tableX-ss.rtf", rtf cells("mean(fmt(%06.2fc)) sd min max")  replace/*
	*	*/ compress  mtitles title("Sample Statistics") stats(N , fmt(2 ) labels( "Observations" )) label

		

			* TABLE 2 *
 ***********************************************
*** item characteristics of PHQ4 and SF-12 ****
***********************************************

alpha sf12*, item gen(sf12score)
alpha phq1-phq4, item gen(phq4score)

	**** alpha for physical and mental scales separately SF-12 ***
	
	alpha sf12_social sf12_quant_s sf12_care_s sf12_energy sf12_relax sf12_down, item gen(sf12mhscore)
	alpha sf12_srh sf12_stairs sf12_hinder sf12_less_b sf12_content sf12_energy sf12_pain sf12_social, item gen(sf12phscore)

	
	label variable sf12score "SF-12 total"
	label variable sf12mhscore "SF-12 mental"
	label variable sf12phscore "SF-12 physical"
	label variable phq4score "PHQ-4"

estpost summarize sf12score sf12mhscore sf12phscore sf12_srh-sf12_social phq4score phq1-phq4,detail
alpha sf12*, item 
scalar alphasf12 = r(alpha) 
estadd scalar  alphasf12
alpha phq1-phq4, item 
scalar alphaphq4 = r(alpha) 
estadd scalar  alphaphq4
*alpha lone1-lone3, item gen(lonescore)
*scalar alphalone = r(alpha) 
*estadd scalar  alphalone
est store items




	
		 esttab  items using "${TABLES}DMHM-MI-table2-items-new.rtf", rtf cells("mean(fmt(%06.2fc)) sd skewness kurtosis min max")  replace/*
		*/ compress  mtitles title("Sample Statistics") stats(N  alphasf12 alphaphq4, fmt(2 %06.2fc %06.2fc %06.2fc) labels( "Observations" "Cronbach's alpha SF12" "Cronbach's alpha PHQ4" "Cronbach's alpha Loneliness" )) label


		
		
***********************************************
*** Summary score by demographic variables ****
***                                        ****
***********************************************






	*** TABLE 7 ***

ttest phq4score, by(sex)

recode migstat (1/2=1) (3=2) (4=3),gen(ms)
label define ms 1 "Germans" 2 "Migrants" 3 "Refugees"
label values ms ms
label variable ms ""


cap erase "${TABLES}DMHM-MI-table7-scores.rtf"



	
foreach X in ms  {
	foreach dv in phq4score sf12score sf12mhscore sf12phscore  {
		foreach sex in 1 2  {	
			cap mat drop SD
			mat SD = J(1,3,.)
			anova `dv' i.`X' if sex ==`sex' [aweight=phrf]
			
			scalar F =  e(F)
			scalar pvalue = Ftail(`e(df_m)',`e(df_r)',`e(F)')
			
			*** ETA^2 ***
			estat esize
			scalar eta2 = `e(r2)'	
			*** Cohen's D ****
			foreach V in 3 2  {
				esize twosample `dv' if ms !=`V' & sex ==`sex' , by(ms) all
				scalar cohensd`V' = r(d)
				 
			}
			foreach V in 1 2 3 {
				sum `dv' if ms ==`V' & sex ==`sex'
				mat SD[1,`V'] = `r(sd)'
			}		
			mat colnames SD = "1.ms" "2.ms" "3.ms"
			margins , over(`X') post	
			estadd scalar eta2
			estadd scalar F
			estadd scalar pvalue
			estadd matrix SD
			estadd scalar cohensd3
			estadd scalar cohensd2
			est store `dv'_`X'`sex'
		}
	}
			esttab phq4score_`X'1 phq4score_`X'2 /// 
			sf12score_`X'1 sf12score_`X'2  /// 
			sf12mhscore_`X'1 sf12mhscore_`X'2  /// 
			sf12phscore_`X'1 sf12phscore_`X'2  /// 
using "${TABLES}DMHM-MI-table7-scores.rtf", append rtf noobs nonumbers  depvars cells("b(fmt(%06.3fc)) SD(fmt(%06.3fc) par)") nostar compress  ///
 label stats(N F pvalue eta2 cohensd3 cohensd2, fmt(0 %06.2fc %06.3fc %06.3fc %06.3fc %06.3fc) labels( "Observations" "F-statistic" "p-value" "Eta^2" "Cohen's d (Mig.vs.Germ)" "Cohen's d (Ref.vs.Germ)" )) ///
 mgroups("PHQ-4" "SF-12 Total" "SF12-MH" "SF12-PH", pattern(1 0  1 0   1 0  1 0 )) mtitles( "Men" "Women" "Men" "Women" "Men" "Women" "Men" "Women") nonumbers
}



*******************************************
***** GENDER DIFFERENCES for APPENDIX *****
*******************************************
est clear
cap erase "${TABLES}DMHM-MI-tableAX-scores-gender.rtf"
foreach X in sex {
	foreach dv in phq4score sf12score sf12mhscore sf12phscore {
		foreach migstat in 1 2 3 {	
			cap mat drop SD
			mat SD = J(1,2,.)
			anova `dv' i.`X' if ms ==`migstat'
			scalar F =  e(F)
			scalar pvalue = Ftail(`e(df_m)',`e(df_r)',`e(F)')
			estat esize
			scalar eta2 = `e(r2)'
			foreach V in 1 2 {
				sum `dv' if sex ==`V' & ms ==`migstat'
				mat SD[1,`V'] = `r(sd)'
			}		
			*** Cohen's D ****
			
				esize twosample `dv' if ms ==`migstat', by(sex) all
				scalar cohensd = r(d)
				 
			
			mat colnames SD = "1.sex" "2.sex"
			margins , over(`X') post	
			estadd scalar eta2
			estadd scalar cohensd
			estadd scalar F
			estadd scalar pvalue
			estadd matrix SD
			est store `dv'_`X'`migstat'
		}
	}
			esttab phq4score_`X'1 phq4score_`X'2 phq4score_`X'3 /// 
			sf12score_`X'1 sf12score_`X'2  sf12score_`X'3 ///
			sf12mhscore_`X'1 sf12mhscore_`X'2 sf12mhscore_`X'3 /// 
			sf12phscore_`X'1 sf12phscore_`X'2 sf12phscore_`X'3 /// 
using "${TABLES}DMHM-MI-tableAX-scores-gender.rtf", replace rtf noobs nonumbers  depvars cells("b(fmt(%06.3fc)) SD(fmt(%06.3fc) par)") nostar compress  ///
 label stats(N F pvalue eta2 cohensd, fmt(0 %06.2fc %06.4fc %06.4fc %06.4fc) labels( "Observations" "F-statistic" "p-value" "Eta^2" "Cohen's d")) ///
 mgroups("PHQ-4" "SF-12-Total" "SF12-MH" "SF12-PH", pattern(1 0 0 1 0 0 1 0 0 1 0 0)) mtitles("Germans" "Migrants" "Refugees" "Germans" "Migrants" "Refugees" "Germans" "Migrants" "Refugees" "Germans" "Migrants" "Refugees") nonumbers
}




*set trace on
levelsof(group_analysis), local(gal)
foreach X in group_analysis  {
	foreach dv in phq4score sf12score sf12mhscore sf12phscore  {
		foreach sex in 1 2  {	
			cap mat drop SD
			mat SD = J(1,17,.)
			anova `dv' i.`X' if sex ==`sex' [aweight=phrf]
			scalar F =  e(F)
			scalar pvalue = Ftail(`e(df_m)',`e(df_r)',`e(F)')
			
			estat esize
			scalar eta2 = `e(r2)'
			local t = 1
			foreach V in  `gal' {
				sum `dv' if `X' ==`V' & sex ==`sex'
				if "`r(N)'" == "0" {
					mat SD[1,`t'] = .
				}
				else {
					mat SD[1,`t'] = `r(sd)'
				}
				local ++t
			}		
			mat colnames SD = "1.group_analysis" "2.group_analysis" "4.group_analysis" "5.group_analysis" "21.group_analysis" "22.group_analysis" "29.group_analysis" "30.group_analysis" "32.group_analysis" "43.group_analysis" "60.group_analysis" "74.group_analysis" "78.group_analysis" "85.group_analysis" "89.group_analysis" "140.group_analysis" 
			margins , over(`X') post	
			estadd scalar eta2
			estadd scalar F
			estadd scalar pvalue
			estadd matrix SD
			est store `dv'_`X'`sex'
		}
	}
			esttab phq4score_`X'1 phq4score_`X'2 /// 
			sf12score_`X'1 sf12score_`X'2  /// 
			sf12mhscore_`X'1 sf12mhscore_`X'2  /// 
			sf12phscore_`X'1 sf12phscore_`X'2  /// 
using "${TABLES}DMHM-MI-table8-scores-countries.rtf", replace rtf noobs nonumbers  depvars cells("b(fmt(%06.3fc)) SD(fmt(%06.3fc) par)") nostar compress  ///
 label stats(N F pvalue eta2, fmt(0 %06.2fc %06.4fc %06.4fc) labels( "Observations" "F-statistic" "p-value" "Eta^2" )) ///
 mgroups("PHQ-4" "SF-12 Total" "SF12-MH" "SF12-PH", pattern(1 0  1 0   1 0  1 0 )) mtitles( "Men" "Women" "Men" "Women" "Men" "Women" "Men" "Women") nonumbers
}


estpost summarize sf12score sf12mhscore sf12phscore sf12* phq4score phq1-phq4,detail

