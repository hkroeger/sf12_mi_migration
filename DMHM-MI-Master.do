*********************************
* Hannes Kröger & Ana Tibubos   *
* master file 2019/12/16		*
* Version submitted to PAS      *
* 								*
* DMHM-MI      Masterfile for   *
* data preparation SOEP         *
* establishing measurement      *
* invariance for SF-12 PHQ-4    *
* across German host population *
* migrants and refugees         *
* 				   				*
*********************************



set more off

clear matrix
clear all
set maxvar 20000
set more off
cap log close 




 if "`c(username)'" == "bootstrap" {
	global DROPBOX    		"/Users/bootstrap/ownCloud/"
	global TEMP           	"/temp/"
	global TABLES           "/Users/bootstrap/Dropbox/WorkinProgress/DMHM/Articles/MI/First/RR/tables/"
	global GRAPHS           "/Users/bootstrap/Dropbox/WorkinProgress/DMHM/Articles/MI/First/RR/graphs/"
	global SOEP         	"/users/bootstrap/documents/data/SOEP34/raw/"
	global SOEPlong    		"/users/bootstrap/documents/data/SOEP34/"
	global DO 				"/users/bootstrap/documents/do/DMHM/MI/"
 } 
 else  if "`c(username)'" == "hkroeger" {
	global DROPBOX    "/Users/nilskruse/ownCloud/"
	global TEMP           "H:/temp/"
	global DATA           "/Data/"
	global SOEP         "S:/DATA/soep33.1_de/stata/"
	global SOEPlong     "S:/DATA/soep33.1_de_l/stata/"

 } 
  else  if "`c(username)'" == "Lea" {
	global DROPBOX    "/Users/lea/ownCloud/"
	global TEMP           "/temp/"
	global DATA           "/Data/"
	global SOEP         "/SOEP32/"
	global SOEPlong     "SOEPlong32/"

 } 
 else if "`c(username)'" == "ANAS USERNAME" {
	global DROPBOX    "/Users/nilskruse/ownCloud/"
	global TEMP           "/temp/"
	global DATA           "/Data/"
	global SOEP         "/SOEP32/"
	global SOEPlong     "SOEPlong32/"

 }
 

 
 
 *******************************
 ***** GETTING DATA     ********
 *******************************
 do "${DO}DMHM-MI-Data.do"
 
 *******************
 **** SOEP-CORE ****
 *******************
  do "${DO}DMHM-MI-Descriptives.do"
 
 
 
 
 
