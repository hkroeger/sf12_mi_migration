# SF12-MI Migration

In this project we tested for measurement invariance of the PHQ-4 and SF-12 questionnaires across refugees, migrants and host population in Germany. Due to different collaboration efforts, data preparation is done in Stata, analyses in R. Specific Alighment analyses were conducted in Mplus, because they were not implemented in R at the time of conducting the study.

The study has been published as: 

Tibubos, A. N., & Kröger, H. (2020). A cross-cultural comparison of the ultrabrief mental health screeners PHQ-4 and SF-12 in Germany. Psychological Assessment, 32(7), 690–697. https://doi.org/10.1037/pas0000814

Access to the data is free of charge and can be pplied for for academic use:

https://www.diw.de/en/diw_01.c.601584.en/data_access.html
