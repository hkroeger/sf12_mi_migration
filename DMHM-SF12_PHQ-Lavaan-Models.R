
################################################
### Defining the models for PHQ and SF12   #####
################################################

#### PHQ ####
phq.model <- 'phq=~ NA*phq1 + phq2 + phq3 + phq4 
phq~~1*phq'

#### SF12 ####
sf12.model <- 'MH=~ NA*sf12_social + sf12_quant_s + sf12_care_s + sf12_energy + sf12_relax + sf12_down
PH=~ NA*sf12_srh + sf12_stairs + sf12_hinder + sf12_less_b + sf12_content + sf12_energy + sf12_pain + sf12_social
sf12_stairs ~~ sf12_hinder
sf12_relax ~~ sf12_energy 
sf12_relax ~~ sf12_down
sf12_less_b ~~ sf12_content
MH ~~ 1*MH
PH ~~ 1*PH'


#### LONE ####
lone.model <- 'LONE=~ NA*lone1 + lone2 + lone3
LONE ~~ 1*LONE'

lone.vars <- c("lone1","lone2","lone3")
phq.vars <- c("phq1","phq2","phq3","phq4")
sf12.vars <- c( "sf12_quant_s", "sf12_care_s",  "sf12_relax", "sf12_down", "sf12_srh", "sf12_stairs", "sf12_hinder", "sf12_less_b", "sf12_content","sf12_energy","sf12_pain")
##### simple CFA on the total population ####"sf12_social",

omega(mi.data[mi.data$check_sf12>=1 & is.na(mi.data$group_analysis) ==FALSE])
omega(mi.data[mi.data$check_phq>=1 & is.na(mi.data$group_analysis) ==FALSE])

scaleStructure(mi.data[mi.data$check_sf12>=1 & is.na(mi.data$group_analysis) ==FALSE,c("sf12_social", "sf12_quant_s", "sf12_care_s",  "sf12_relax", "sf12_down", "sf12_srh", "sf12_stairs", "sf12_hinder", "sf12_less_b", "sf12_content","sf12_energy","sf12_pain")])

##### SF-12 #####
sf12.fit <- cfa(sf12.model, data=mi.data[mi.data$check_sf12>=1 & is.na(mi.data$group_analysis) ==FALSE,])

summary(sf12.fit, fit.measures=TRUE)
##### PHQ-4 #####
phq.fit <- cfa(phq.model, data=mi.data[mi.data$check_phq>=1 & is.na(mi.data$group_analysis) ==FALSE,])

summary(phq.fit, fit.measures=TRUE)


gof.names <- c("chisq","df","pvalue",
               "cfi.robust","rmsea.robust","rmsea.ci.lower.robust","rmsea.ci.upper.robust","srmr_mplus")






corigin <- c("Germany","Turkey","Greece","Italy","Romania","Poland","Bulgaria","Syria",
             "Russia","Afghanistan","Iraq","Kasachstan","Ukraine","Pakistan","Eritrea","Kosovo",
             "Germany","Turkey","Greece","Italy","Romania","Poland","Bulgaria","Syria",
             "Russia","Afghanistan","Iraq","Kasachstan","Ukraine","Eritrea","Kosovo")


country.codes.all <- unique(na.omit(mi.data$group_analysis))[1:length(unique(na.omit(mi.data$group_analysis)))]

corigin.unique <- c("Germany","Turkey","Greece","Italy","Romania","Poland","Bulgaria","Syria",
                   "Russia","Afghanistan","Iraq","Kasachstan","Ukraine","Pakistan","Eritrea","Kosovo")

##### fit the models separately for all countries ####

run.model.country <- function(model,country,check){
  cfa(model, sampling.weight="phrf_stand",missing="ML", data=mi.data[check & is.na(mi.data$group_analysis) ==FALSE & mi.data$corigin==country,])
  #cfa(model, estimator="WLSMV", ordered=c(phq.vars,sf12.vars), data=mi.data[check & is.na(mi.data$group_analysis) ==FALSE & mi.data$corigin==country,])
}

run.model.migstat <- function(model,mig,check){
  cfa(model,sampling.weight="phrf_stand",missing="ML", data=mi.data[check & is.na(mi.data$group_analysis) ==FALSE & mi.data$migstat==mig,])
  #cfa(model, estimator="WLSMV",ordered=c(phq.vars,sf12.vars),  data=mi.data[check & is.na(mi.data$group_analysis) ==FALSE & mi.data$migstat==mig,])
}

phq.models.countries <- lapply(country.codes.all,FUN=run.model.country, model=phq.model,check=c(mi.data$check_phq>=1))

sf12.models.countries <- lapply(country.codes.all,FUN=run.model.country, model=sf12.model,check=c(mi.data$check_sf12>=1))

mig.list <- c(1,3,4)

phq.models.migstat <- lapply(mig.list,FUN=run.model.migstat, model=phq.model,check=c(mi.data$check_phq>=1))

sf12.models.migstat <- lapply(mig.list,FUN=run.model.migstat, model=sf12.model,check=c(mi.data$check_sf12>=1))


phq.models.countries <- c(phq.models.migstat,phq.models.countries)
sf12.models.countries <- c(sf12.models.migstat,sf12.models.countries)

#### extract shit ###

names(phq.models.countries)  <- c("Germany","Migrants","Refugees", corigin.unique)
names(sf12.models.countries) <- c("Germany","Migrants","Refugees", corigin.unique)

#newd <- data@fit[threemod,gof.names]

phq.models.countries.fit <- lapply(phq.models.countries,FUN=fitMeasures, fit.measures = "all", baseline.model = NULL)
sf12.models.countries.fit <- lapply(sf12.models.countries,FUN=fitMeasures, fit.measures = "all", baseline.model = NULL)


median.sd.est <- function(model) {
  muh <- standardizedSolution(model)
  print(muh[muh$op=="=~","est.std"])
  #median(abs(as.numeric(muh[muh$op=="=~","est.std"])))
  quantile(abs(as.numeric(muh[muh$op=="=~","est.std"])), prob = c(0.25))
}

countries.phq.sd.median <-  lapply(phq.models.countries,FUN=median.sd.est)

countries.sf12.sd.median <- lapply(sf12.models.countries,FUN=median.sd.est)



reduce.tab.countries.full <- function(data,model,name) {
  newd <- data.frame(t(data[gof.names]))
  rownames(newd) <- c(paste0(name,collapse=""))
  write.table(format(round(newd, 3), nsmall = 3), file = paste0(tables.path,"LAVAANgofstatfull_",model,".txt",collapse=""), sep = ";", quote = FALSE, col.names = FALSE, row.names = TRUE,append=TRUE)
  newd
}
file.remove(paste0(tables.path,"LAVAANgofstatfull_SF12_countriesFULL.txt",collapse=""))
file.remove(paste0(tables.path,"LAVAANgofstatfull_PHQ_countriesFULL.txt",collapse=""))
lapply(seq_along(phq.models.countries.fit), function(k){reduce.tab.countries.full(phq.models.countries.fit[[k]], names(phq.models.countries.fit)[[k]],model="PHQ_countriesFULL")})
lapply(seq_along(sf12.models.countries.fit), function(k){reduce.tab.countries.full(sf12.models.countries.fit[[k]], names(sf12.models.countries.fit)[[k]],model="SF12_countriesFULL")})




###### models without missing, but categorical ####




