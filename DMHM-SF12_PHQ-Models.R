
################################################
### Defining the models for PHQ and SF12   #####
################################################

#### PHQ ####
phq.model <- 'phq=~ NA*phq1 + phq2 + phq3 + phq4 
phq~~1*phq'


### MODEL FITTING SF-12 ###
#  sf12.model.fac.design



##### simple CFA on the total population ####"sf12_social",

omega(select(sf12.phq.data,phq.vars))
omega(select(sf12.phq.data,sf12.vars))



##### SF-12 #####
sf12.fit <- cfa(sf12.model.fac.design,sampling.weight="phrf_stand",missing="ML", data=filter(sf12.phq.data,check_sf12>=1))

summary(sf12.fit, fit.measures=TRUE)
##### PHQ-4 #####
phq.fit <- cfa(phq.model,sampling.weight="phrf_stand",missing="ML", data=filter(sf12.phq.data,check_phq>=1))

summary(phq.fit, fit.measures=TRUE)


gof.names <- c("chisq","df","pvalue",
               "cfi.robust","rmsea.robust","rmsea.ci.lower.robust","rmsea.ci.upper.robust","srmr_mplus")






corigin <- c("Germany","Turkey","Greece","Italy","Romania","Poland","Bulgaria","Syria",
             "Russia","Afghanistan","Iraq","Kasachstan","Ukraine","Pakistan","Eritrea","Kosovo",
             "Germany","Turkey","Greece","Italy","Romania","Poland","Bulgaria","Syria",
             "Russia","Afghanistan","Iraq","Kasachstan","Ukraine","Eritrea","Kosovo")


country.codes.all <- unique(na.omit(sf12.phq.data$group_analysis))[1:length(unique(na.omit(sf12.phq.data$group_analysis)))]

corigin.unique <- c("Germany","Turkey","Greece","Italy","Romania","Poland","Bulgaria","Syria",
                   "Russia","Afghanistan","Iraq","Kasachstan","Ukraine","Pakistan","Eritrea","Kosovo","Other")

##### fit the models separately for all countries ####

run.model.country <- function(model,country,check){
  cfa(model, sampling.weight="phrf_stand",missing="ML", data=filter(sf12.phq.data,corigin==country,check))
}

run.model.migstat <- function(model,mig,check){
  cfa(model,sampling.weight="phrf_stand",missing="ML", data=filter(sf12.phq.data,migstat==mig,check))
}

phq.models.countries <- lapply(country.codes.all,FUN=run.model.country, model=phq.model,check=c(sf12.phq.data$check_phq>=1))

sf12.models.countries <- lapply(country.codes.all,FUN=run.model.country, model=sf12.model.fac.design,check=c(sf12.phq.data$check_sf12>=1))

mig.list <- c(1,3,4)

phq.models.migstat <- lapply(mig.list,FUN=run.model.migstat, model=phq.model,check=c(sf12.phq.data$check_phq>=1))

sf12.models.migstat <- lapply(mig.list,FUN=run.model.migstat, model=sf12.model.fac.design,check=c(sf12.phq.data$check_sf12>=1))


phq.models.countries <- c(phq.models.migstat,phq.models.countries)
sf12.models.countries <- c(sf12.models.migstat,sf12.models.countries)

#### extract shit ###

names(phq.models.countries)  <- c("Germany","Migrants","Refugees", corigin.unique)
names(sf12.models.countries) <- c("Germany","Migrants","Refugees", corigin.unique)

#newd <- data@fit[threemod,gof.names]

phq.models.countries.fit <- lapply(phq.models.countries,FUN=fitMeasures, fit.measures = "all", baseline.model = NULL)
sf12.models.countries.fit <- lapply(sf12.models.countries,FUN=fitMeasures, fit.measures = "all", baseline.model = NULL)


median.sd.est <- function(model) {
  muh <- standardizedSolution(model)
  print(muh[muh$op=="=~","est.std"])
  #median(abs(as.numeric(muh[muh$op=="=~","est.std"])))
  quantile(abs(as.numeric(muh[muh$op=="=~","est.std"])), prob = c(0.25))
}

countries.phq.sd.p25 <-  lapply(phq.models.countries,FUN=median.sd.est) 
countries.phq.sd.p25 <- data.frame(matrix(unlist(countries.phq.sd.p25), nrow=length(countries.phq.sd.p25), byrow=T))
names(countries.phq.sd.p25) <- c("PHQ4-p25")

countries.sf12.sd.p25 <- lapply(sf12.models.countries,FUN=median.sd.est)
countries.sf12.sd.p25 <- data.frame(matrix(unlist(countries.sf12.sd.p25), nrow=length(countries.sf12.sd.p25), byrow=T))
names(countries.sf12.sd.p25) <- c("SF-12-p25")
countries.sd.p25 <- bind_cols(countries.sf12.sd.p25,countries.phq.sd.p25)
rownames(countries.sd.p25) <- c("Host","Migrants","Refugees", corigin.unique)

write.table(format(round(countries.sd.p25, 3), nsmall = 3) , file = paste0(tables.path,"countries-std-p25.txt",collapse=""), sep = ";", quote = FALSE, col.names = TRUE, row.names = TRUE)




##### intercepts and factor loadings for all countries ####
loadings.sd.est <- function(model) {
  muh <- standardizedSolution(model)
  print(muh[muh$op=="=~","est.std"])
  print(format(round(muh[muh$op=="=~","pvalue"], 3), nsmall = 3))
  print(muh[muh$op=="=~","rhs"])
  #median(abs(as.numeric(muh[muh$op=="=~","est.std"])))
  #quantile(abs(as.numeric(muh[muh$op=="=~","est.std"])), prob = c(0.35))
  #min(abs(as.numeric(muh[muh$op=="=~","est.std"])))
  ob <- t(as.matrix(format(round(as.numeric(muh[muh$op=="=~","est.std"]),3), digits=3, nsmall=3)))
  print(ob)
  names(ob) <- muh[muh$op=="=~","rhs"]
  ob
}

intercepts.est <- function(model) {
  muh <- parameterEstimates(model)
  #print(muh[muh$op=="~1","est"])
  #print(format(round(muh[muh$op=="=~","pvalue"], 3), nsmall = 3))
  #median(abs(as.numeric(muh[muh$op=="=~","est.std"])))
  #quantile(abs(as.numeric(muh[muh$op=="=~","est.std"])), prob = c(0.35))
  #min(abs(as.numeric(muh[muh$op=="=~","est.std"])))
  
  ob <- t(as.matrix(format(round(as.numeric(muh[muh$op=="~1" & muh$lhs != "phq" & muh$lhs != "PH" & muh$lhs != "MH","est"]),3), digits=3, nsmall=3)))
  print(ob)
  names(ob) <- muh[muh$op=="~1"& muh$lhs != "phq" & muh$lhs != "PH" & muh$lhs != "MH","lhs"]
  ob
}

#################
##### PHQ4 ######
#################

countries.phq.sd.loadings <-  sapply(phq.models.countries,FUN=loadings.sd.est) 
countries.sf12.sd.loadings <-  sapply(sf12.models.countries,FUN=loadings.sd.est)

countries.sd.loadings <- rbind(countries.phq.sd.loadings,countries.sf12.sd.loadings) %>% as.data.frame %>%
  rownames_to_column() %>% extract(rowname, c("A"), "12_([[:alnum:]]+)_",remove = FALSE, convert=TRUE)  %>% arrange(A) %>%
  select(-A)


countries.phq.intercepts <-  sapply(phq.models.countries,FUN=intercepts.est)
countries.sf12.intercepts <-  sapply(sf12.models.countries,FUN=intercepts.est)

countries.intercepts <- rbind(countries.phq.intercepts,countries.sf12.intercepts) %>% as.data.frame %>%
  rownames_to_column() %>% extract(rowname, c("A"), "12_([[:alnum:]]+)_",remove = FALSE, convert=TRUE)  %>% arrange(A) %>%
  select(-A)


write.table(countries.sd.loadings, file = paste0(tables.path,"countries-std.txt",collapse=""), sep = ";", quote = FALSE, col.names = TRUE, row.names = FALSE,append=FALSE)

write.table(countries.intercepts, file = paste0(tables.path,"countries-intercepts.txt",collapse=""), sep = ";", quote = FALSE, col.names = TRUE, row.names = FALSE,append=FALSE)






##################
##### SF-12 ######
##################


countries.sf12.intercepts <-  sapply(sf12.models.countries,FUN=intercepts.est)

countries.sf12.std <- as.data.frame(t(countries.sf12.sd.loadings))
countries.sf12.intercepts <- as.data.frame(t(countries.sf12.intercepts))

write.table(format(round(countries.sf12.std, 3), nsmall = 3), file = paste0(tables.path,"countries-SF-12-std.txt",collapse=""), sep = ";", quote = FALSE, col.names = FALSE, row.names = TRUE,append=FALSE)

write.table(format(round(countries.sf12.intercepts, 3), nsmall = 3), file = paste0(tables.path,"countries-SF-12-intercepts.txt",collapse=""), sep = ";", quote = FALSE, col.names = FALSE, row.names = TRUE,append=FALSE)






### MODEL FIT OUTPUT ######


reduce.tab.countries.full <- function(data,model,name) {
  newd <- data.frame(t(data[gof.names]))
  rownames(newd) <- c(paste0(name,collapse=""))
  write.table(format(round(newd, 3), nsmall = 3), file = paste0(tables.path,"LAVAANgofstatfull_",model,".txt",collapse=""), sep = ";", quote = FALSE, col.names = FALSE, row.names = TRUE,append=TRUE)
  newd
}
file.remove(paste0(tables.path,"LAVAANgofstatfull_SF12_countriesFULL.txt",collapse=""))
file.remove(paste0(tables.path,"LAVAANgofstatfull_PHQ_countriesFULL.txt",collapse=""))
lapply(seq_along(phq.models.countries.fit), function(k){reduce.tab.countries.full(phq.models.countries.fit[[k]], names(phq.models.countries.fit)[[k]],model="PHQ_countriesFULL")})
lapply(seq_along(sf12.models.countries.fit), function(k){reduce.tab.countries.full(sf12.models.countries.fit[[k]], names(sf12.models.countries.fit)[[k]],model="SF12_countriesFULL")})


